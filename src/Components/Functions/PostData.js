import defaultLink from "./Links";
async function PostData(data, link){
    try {
        const response = await fetch(defaultLink + link,{
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const json = await response.json();
        return(JSON.stringify(json));
    }catch{
        return("Error");
    }
}
export default PostData;