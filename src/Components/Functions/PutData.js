import defaultLink from "./Links";
async function PutData(data, link){
    try {
        const response = await fetch(defaultLink + link,{
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        const json = await response.json();
        return(JSON.stringify(json));
    }catch{
        return("Error");
    }
}
export default PutData;