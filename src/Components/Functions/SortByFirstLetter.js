function SortByFirstLetter(array){
    return array
        .sort((a, b) => a.name.localeCompare(b.name))
        .reduce((r, e) => {
            const key = e.name[0].toUpperCase();
            if (!r[key])
                r[key] = [];
            r[key].push(e);
            return r;
        }, {});

}
export default SortByFirstLetter;