import defaultLink from "./Links";
async function DeleteData(link){
    try {
        const response = await fetch(defaultLink + link,{
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
        });
        const json = await response.json();
        return(JSON.stringify(json));
    }catch{
        return("Error");
    }
}
export default DeleteData;