import React, {useState, useEffect} from "react";
import {Button, Form, Modal, NavDropdown} from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { Typeahead } from 'react-bootstrap-typeahead';
import { fetchFaculties } from '../../Store/Actions';
import PostData from "../../Functions/PostData";

function AddSpecialty(){
    const faculties = useSelector((state)=>state.faculties);
    const dispatch = useDispatch();
    useEffect(()=>{
        if (!faculties.length) {
            dispatch(fetchFaculties());
        }
    })
    const [show, setShow] = useState(false);
    const [name, setName] = useState('');
    const [acronym, setAcronym] = useState('');
    const [description, setDescription] = useState('');
    const [faculty, setFaculty] = useState([]);

    const [validated, setValidated] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => {
        setShow(true);
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }else{
            const data={
                name: name,
                facId: faculty[0].id,
                acronym: acronym,
                description: description
            }
            PostData(data, "/api/specialty").then()
        }
        setValidated(true);
    };

    return(
        <>
            <NavDropdown.Item className="custom-nav-dropdown" onClick={handleShow}>Специальность</NavDropdown.Item>
            <Modal
                size="lg"
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{paddingTop: "2rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Добавить специальность</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="addSpecialty" noValidate validated={validated} onSubmit={handleSubmit}>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Typeahead
                                id="add-specialty"
                                labelKey="name"
                                onChange={setFaculty}
                                options={faculties}
                                placeholder="Институт"
                                selected={faculty}
                                inputProps={{ required: true }}
                                emptyLabel="Совпадений не найдено"
                            />
                        </Form.Group>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Название" required value={name}
                                          onChange={(e) => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Сокращение" value={acronym}
                                          onChange={(e) => setAcronym(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control as="textarea" rows={8} placeholder="Описание" value={description}
                                          onChange={(e) => setDescription(e.target.value)}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addSpecialty" variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button form="addSpecialty" variant="dark" type="submit">Добавить</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddSpecialty;