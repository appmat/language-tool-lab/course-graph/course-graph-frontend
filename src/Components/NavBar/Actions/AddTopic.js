import React, {useState} from "react";
import {NavDropdown, Modal, Button, Form} from "react-bootstrap";
import PostData from "../../Functions/PostData";

function AddTopic(){
    const [show, setShow] = useState(false);
    const [name, setName] = useState('');
    const [acronym, setAcronym] = useState('');
    const [description, setDescription] = useState('');

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [validated, setValidated] = useState(false);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }else{
            const data={
                name: name,
                acronym: acronym,
                description: description
            };
            PostData(data, "/api/subject").then(res=>{
                console.log(res)
            })
        }
        setValidated(true);
    };

    return(
        <>
            <NavDropdown.Item className="custom-nav-dropdown" onClick={handleShow}>Тема</NavDropdown.Item>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Добавить тему</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="addTopic" noValidate validated={validated} onSubmit={handleSubmit}>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Название" required value={name}
                                          onChange={(e) => setName(e.target.value)}/>

                        </Form.Group>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Сокращение" value={acronym}
                                          onChange={(e) => setAcronym(e.target.value)} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Control as="textarea" rows={5} placeholder="Описание" value={description}
                                          onChange={(e) => setDescription(e.target.value)} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addTopic" variant="outline-dark" type="reset" onClick={handleClose}> Отмена </Button>
                    <Button form="addTopic" variant="dark" type="submit" >Добавить</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddTopic;