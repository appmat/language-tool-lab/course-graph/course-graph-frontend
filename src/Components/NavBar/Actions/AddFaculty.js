import React, {useState} from "react";
import {NavDropdown, Modal, Button, Form} from "react-bootstrap";
import PostData from "../../Functions/PostData";

function AddFaculty(){
    const [show, setShow] = useState(false);
    const [name, setName] = useState('');
    const [acronym, setAcronym] = useState('');

    const [validated, setValidated] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }else{
            const data={ name: name };
            PostData(data, "/api/faculty").then(res=>{
                console.log(res);
            })
        }
        setValidated(true);
    };

    return(
        <>
            <NavDropdown.Item className="custom-nav-dropdown" onClick={handleShow}>Факультет</NavDropdown.Item>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{paddingTop: "4rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Добавить факультет</h4>
                </Modal.Header>
                <Modal.Body style={{justifyContent: "center", paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="addFaculty" noValidate validated={validated} onSubmit={handleSubmit} >
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Название" required value={name}
                                          onChange={(e) => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Сокращение" value={acronym}
                                          onChange={(e) => setAcronym(e.target.value)}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="addFaculty" variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button form="addFaculty" variant="dark" type="submit">Добавить</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default AddFaculty;