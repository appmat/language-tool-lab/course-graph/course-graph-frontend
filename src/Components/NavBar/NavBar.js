import React from "react";
import {Navbar, Nav, NavDropdown} from "react-bootstrap";
import './NavBar.css'

import AddFaculty from "./Actions/AddFaculty";
import AddSpecialty from "./Actions/AddSpecialty";
import AddCourse from "./Actions/AddCourse";
import AddTopic from "./Actions/AddTopic";

function NavBar() {
    return(
        <Navbar bg="light">
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav" >
                <Nav className="ms-auto">
                    <Nav.Link href="/">Специальности</Nav.Link>
                    <Nav.Link href="/courses">Курсы</Nav.Link>
                    <Nav.Link href="/topics">Темы</Nav.Link>
                    <NavDropdown alignRight title="Добавить" id="responsive-nav-dropdown" className="nav-dropdown" >
                        <AddFaculty/>
                        <AddSpecialty/>
                        <AddCourse/>
                        <AddTopic/>
                    </NavDropdown>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default NavBar;