import React, {useState} from "react";
import {Modal, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import DeleteData from "../../Functions/DeleteData";

function DeleteSpecialty(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleSubmit = () => {
        DeleteData(`/api/specialty/${props.id}`).then(() => {
        })
    };

    return (
        <>
            <Button variant="light" className="edit" onClick={handleShow}
                    style={{/*display: "none",*/ backgroundColor: "transparent", border: "none"}}>
                <i className="fas fa-trash"/>
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{paddingTop: "6rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Удалить</h4>
                </Modal.Header>
                <Modal.Body style={{paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <h6 style={{textAlign: "center"}}>{props.name}</h6>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Link to="/" className="specialty-name">
                        <Button variant="dark" onClick={handleSubmit}>Удалить</Button>
                    </Link>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default DeleteSpecialty;