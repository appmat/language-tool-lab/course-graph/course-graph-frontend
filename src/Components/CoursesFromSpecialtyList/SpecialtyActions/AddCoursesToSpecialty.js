import React, {useState, useEffect} from "react";
import {Button, Form, Modal, Row, Col, Container} from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import { useSelector, useDispatch } from "react-redux";
import { fetchCourses } from "../../Store/Actions";
import PutData from "../../Functions/PutData";

import 'react-bootstrap-typeahead/css/Typeahead.css';

function AddCoursesToSpecialty(props){
    const terms=[1,2,3,4,5,6,7,8];
    const specialtyId = props.specialtyId;

    // получение списка всех курсов
    const allCourses = useSelector((state)=>state.courses);
    const dispatch = useDispatch();

    const [avCourses, setAvCourses] = useState([]);
    const [course, setCourse] = useState([]);
    const [term, setTerm] = useState("...");
    const [credits, setCredits] = useState('');

    // выбранные курсы
    const [items, setItems]=useState(props.currentCourses);

    // получение списка курсов, которых нет
    const getAvailableCourses = () => {
        let currentCourses=[];
        let availableCourses=[];
        items.forEach(i => {
            currentCourses.push(i.id);
        })
        //console.log(currentCourses)
        allCourses.forEach(i => {
            // если id просматриваемого элемента нет в текущих курсах, пушим в доступные
            if (!currentCourses.includes(i.id))
                availableCourses.push(i);
        })
        //console.log(availableCourses)
        setAvCourses(availableCourses);
    }

    useEffect(() => {
        if (!allCourses.length){
            dispatch(fetchCourses());
        }
        getAvailableCourses();
    },[allCourses.length, items]);

    // добавление курсов в список
    const handleSubmit = () => {
        if (course.length>0 && term!=="..." && credits !== ''){
            // сохранение введенных данных
            const data = {
                id:course[0].id,
                name: course[0].name,
                term: Number(term),
                credits: Number(credits),
                isNew: true
            };
            console.log(data);
            if (items.findIndex(item => item.id===data.id && item.term===data.term && item.name===data.name)===-1){
                setItems([data, ...items]);
            }
            // очистка поля ввода
            setCourse([]);
            setTerm("...");
            setCredits('');
        }
    };
    // удаление из списка
    const deleteItem = (id, term) => {
        const newItems=items.filter(item => item.id !==id || item.term!==term);
        setItems(newItems);
        //getAvailableCourses();
    }

    // открытие/закрытие модального окна
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        setItems(props.currentCourses);
    }
    const handleShow = () => {
        setShow(true);

        getAvailableCourses();
    }

    const handlePut=()=>{
        let courses=[];
        items.forEach(item=>{
            const data={
                courseId: item.id,
                term: item.term,
                credits: item.credits
            }
            courses.push(data);
        })
        PutData(courses, `/api/specialty/${specialtyId}/courses`).then(()=>{
        });
    }

    return(
        <div style={{alignItems: "center", display: "flex", justifyContent: "center"}}>
            <Button variant={"light"}
                    style={{margin: "0 .5rem 0 .5rem"}}
                    onClick={handleShow}>
                Изменить курсы
            </Button>
            <Modal show={show}
                   onHide={handleClose}
                   backdrop="static"
                   keyboard={false}
                   style={{paddingTop: "2rem"}}
                   size="xl"
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "center", paddingTop: "2rem"}}>
                    <h4>Курсы специальности</h4>
                </Modal.Header>
                <Modal.Body>
                    <div style={{display: "flex", justifyContent: "center"}}>
                        <Form id="add-course-to-specialty" onSubmit={handlePut}>
                            <Form.Group>
                                <div style={{display: "flex"}}>
                                    <Typeahead
                                        id="add-course-to-specialty"
                                        labelKey="name"
                                        onChange={setCourse}
                                        options={avCourses} // allCourses
                                        placeholder="Курс"
                                        selected={course}
                                        emptyLabel="Совпадений не найдено"
                                        style={{width: "33rem"}}
                                    />
                                    <Form.Control title="Семестр" as="select"
                                                  style={{maxWidth: "4rem", margin: "0 1rem 0 1rem"}}
                                                  value={term}
                                                  onChange={(e) => setTerm(e.target.value)}
                                    >
                                        <option>...</option>
                                        {terms.map((t)=>{
                                            return(
                                                <option key={t} value={t}>{t}</option>
                                            )
                                        })}
                                    </Form.Control>
                                    <Form.Group>
                                        <Form.Control type="text" title="Зачетные единицы"
                                                      style={{maxWidth: "4rem", margin: "0 1rem 0 0"}}
                                                      value={credits}
                                                      onChange={(e) => setCredits(e.target.value)}/>
                                    </Form.Group>
                                    <Button variant="outline-dark"
                                            onClick={(e)=>{e.preventDefault(); handleSubmit()}}
                                    >
                                        Добавить
                                    </Button>
                                </div>
                            </Form.Group>
                        </Form>
                    </div>

                    <Container style={{marginTop: "2rem"}}>
                        <Row style={items.length===0 ? {display: "none"} :
                            {alignItems: "center", textAlign: "center", paddingBottom: "1rem"}}
                        >
                            <Col lg="3"/>
                            <Col lg="3" style={{fontWeight: "bold"}}>Название</Col>
                            <Col lg="2" style={{fontWeight: "bold"}}>Семестр</Col>
                            <Col lg="1" style={{fontWeight: "bold"}}>З.Ед.</Col>
                        </Row>
                    </Container>

                    <Container style={{maxHeight: "calc(100vh - 450px)", overflowY: "auto", marginTop: ".25rem", paddingLeft: "0"}}>
                        {items.map((item,id)=>{
                            return(
                                <Row key={id} style={{alignItems: "center", paddingBottom: ".5rem", textAlign: "center", paddingTop: "5px"}}>
                                    <Col lg="1">

                                    </Col>
                                    <Col xs lg="2" style={{textAlign: "right"}}>
                                        {id+1}
                                    </Col>
                                    <Col lg="6" style={{textAlign: "left", minWidth: "13rem", maxWidth: "22rem"}}>
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            <div>{item.name}</div>
                                            <div style={item.isNew ?
                                                {width: "20px", height: "20px",borderRadius: "0.25rem", backgroundColor: "rgb(233,233,234)", marginLeft: "10px"}
                                                : {}}/>
                                        </div>

                                    </Col>
                                    <Col xs lg="1" style={{textAlign: "left"}}>{item.term}</Col>
                                    <Col xs lg="1" style={{textAlign: "right"}}>{item.credits}</Col>
                                    <Col xs lg="1">
                                        <Button variant="light" onClick={()=>deleteItem(item.id, item.term)}
                                                style={{backgroundColor: "transparent"}}>
                                            <i className="fas fa-trash"/>
                                        </Button>
                                    </Col>
                                </Row>
                            )
                        })}
                    </Container>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button variant="dark" type="submit" form="add-course-to-specialty" >Готово</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default AddCoursesToSpecialty;
