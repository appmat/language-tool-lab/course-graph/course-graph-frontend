import React, {useState} from "react";
import {Modal, Button, Form} from "react-bootstrap";
import PutData from "../../Functions/PutData";

function RenameSpecialty(props){
    const [show, setShow] = useState(false);
    const [name, setName] = useState(props.specialty.name);
    const [acronym, setAcronym] = useState(props.specialty.acronym);
    const [description, setDescription] = useState(props.specialty.description);
    const [validated, setValidated] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }else{
            const data={
                id: props.id,
                name: name,
                facId: props.facId,
                acronym: acronym,
                description: description
            };
            PutData(data, `/api/specialty/${data.id}`).then(()=>{
            })
        }
        setValidated(true);
    };

    return(
        <>
            <Button title="Редактировать описание" variant="light" className="edit" onClick={handleShow} style={{backgroundColor: "transparent", border: "none"}}>
                <i className="fas fa-pen"/>
            </Button>
            <Modal
                show={show}
                size="lg"
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{paddingTop: "2rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Редактирование</h4>
                </Modal.Header>
                <Modal.Body style={{paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="renameSpecialty" noValidate validated={validated} onSubmit={handleSubmit}>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Название" required value={name}
                                          onChange={(e) => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Сокращение" value={acronym}
                                          onChange={(e) => setAcronym(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control as="textarea" rows={8} placeholder="Описание" value={description}
                                          onChange={(e) => setDescription(e.target.value)}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="renameSpecialty" variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button form="renameSpecialty" variant="dark" type="submit">Сохранить</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default RenameSpecialty;