import React from "react";
import './CoursesFromSpecialty.css';
import {Col, Container, Row} from "react-bootstrap";

import AddCoursesToSpecialty from "./SpecialtyActions/AddCoursesToSpecialty";

import CoursesFromSpecialtyView from "./CoursesFromSpecialtyView";
import SpecialtyHeader from "./SpecialtyHeader";
import defaultLink from "../Functions/Links";

async function getCourses(id) {
    const specialtyInfo = await fetch(`${defaultLink}/api/specialty/${id}`).then(response => response.json());
    const coursesInfo = await fetch(`${defaultLink}/api/specialty/${id}/courses`).then(response => response.json());
    return [specialtyInfo.specialty,coursesInfo];
}

class CoursesFromSpecialtyList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            specialty: {},
            specialtyCourses: [],
            isLoaded: false
        };
    }

    handleLoaded = () => {
        this.setState({isLoaded: true})
    }

    componentDidMount() {
        getCourses(this.props.id)
            .then((res) => {
                this.setState({
                    specialty: res[0],
                    specialtyCourses: res[1]
                }, this.handleLoaded)
            })

    }

    render() {
        const {specialty, specialtyCourses, isLoaded} = this.state;
        if (!isLoaded) {
            return <p/>
        } else {
            document.title = "Специальность: " + specialty.name;
            return (
                <div className="specialty-page">
                    <div className="courses-from-specialty-list-title">
                        <Container style={{margin: "0", padding: "0", maxWidth: "87.75rem"}}>
                            <SpecialtyHeader specialty={specialty}/>
                            <Row style={{alignItems: "center", marginTop: "1rem"}}>
                                <Col style={{paddingRight: "0"}}>
                                    <AddCoursesToSpecialty specialtyId={specialty.id}
                                                               currentCourses={specialtyCourses}/>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <CoursesFromSpecialtyView items={specialtyCourses}/>
                </div>
            );
        }
    }
}

export default CoursesFromSpecialtyList;