import React from "react";
import {Button, Col, Row} from "react-bootstrap";
import {Link} from "react-router-dom";
import RenameSpecialty from "./SpecialtyActions/RenameSpecialty";
import DeleteSpecialty from "./SpecialtyActions/DeleteSpecialty";

function SpecialtyHeader(props){
    const specialty = props.specialty;
    return(
        <Row style={{alignItems: "center"}}>
            <Col xs lg="1">
                <Link to="/">
                    <Button variant="outline-dark">
                        Назад
                    </Button>
                </Link>
            </Col>
            <Col>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <h4 style={{textAlign: "right", wordBreak: "break-word"}}>
                        {specialty.name + (specialty.acronym.length ? (" (" + specialty.acronym + ")") : '')}
                    </h4>
                    <RenameSpecialty id={specialty.id} specialty={specialty}
                                     facId={specialty.facId}/>
                    <DeleteSpecialty id={specialty.id} name={specialty.name}/>
                </div>
            </Col>
        </Row>
    )
}

export default SpecialtyHeader;