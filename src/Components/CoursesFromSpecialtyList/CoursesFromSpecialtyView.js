import React from "react";
import {Link} from "react-router-dom";

function CoursesFromSpecialtyView(props){
    const items=props.items;
    return(
        <div className="courses-from-specialty-list" style={{maxWidth: "90rem"}}>
            {items.map((item,id) => {
                return (
                    <Link key={id} to={`/courses/${item.id}`} target="_blank" className="specialty-name">
                        <div className="course-card" key={id} title={item.name}>
                            <div className="course-caption">
                                {item.name}
                            </div>
                            <div>
                                <p style={{marginBottom: 0}}><i>Семестр</i>: {item.term}</p>
                                <p><i>З.Е.</i>: {item.credits}</p>
                            </div>
                        </div>
                    </Link>
                )
            })}
        </div>
    );

}
export default CoursesFromSpecialtyView;