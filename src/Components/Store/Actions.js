import defaultLink from "../Functions/Links";
export function fetchFaculties() {
    return (dispatch) => {
        fetch(`${defaultLink}/api/faculty`)
            .then(res=>{
                if (!res.ok){
                    throw new Error(res.statusText);
                }
                return res;
            })
            .then(res=>res.json())
            .then(faculties=>dispatch(fetchFacultiesSuccess(faculties)))
            .catch();
    }
}

export function fetchFacultiesSuccess(faculties){
    faculties = faculties.sort((a, b) => a.name.localeCompare(b.name));
    return {
        type: 'FETCH_DATA_FACULTIES',
        faculties
    }
}


export function fetchSpecialties(id) {
    return (dispatch) => {
        fetch(`${defaultLink}/api/faculty/${id}/specialties`)
            .then(res=>{
                if (!res.ok){
                    throw new Error(res.statusText);
                }
                return res;
            })
            .then(res=>res.json())
            .then(specialties=>dispatch(fetchSpecialtiesSuccess(specialties)))
            .catch();
    }
}

export function fetchSpecialtiesSuccess(specialties){
    return {
        type: "FETCH_DATA_SPECIALTIES",
        specialties: specialties
    };
}

export function fetchCourses() {
    return (dispatch) => {
        fetch(`${defaultLink}/api/course`)
            .then(res=>{
                if (!res.ok){
                    throw new Error(res.statusText);
                }
                return res;
            })
            .then(res=>res.json())
            .then(courses=>dispatch(fetchCoursesSuccess(courses)))
            .catch();
    }
}

export function fetchCoursesSuccess(courses){
    //console.log(courses);
    return {
        type: "FETCH_DATA_COURSES",
        courses: courses
    };
}

export function fetchTopics() {
    return (dispatch) => {
        fetch(`${defaultLink}/api/subject`)
            .then(res=>{
                if (!res.ok){
                    throw new Error(res.statusText);
                }
                return res;
            })
            .then(res=>res.json())
            .then(topics => {
                dispatch(fetchTopicsSuccess(topics));
            })
            .catch();
    }
}

export function fetchTopicsSuccess(topics){
    return {
        type: 'FETCH_DATA_TOPICS',
        topics: topics
    }
}

