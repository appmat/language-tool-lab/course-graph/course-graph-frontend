const initialState={
    faculties: [],
    specialties: [],
    courses: [],
    topics: []
}

export const rootReducer = (state=initialState, action) => {
    switch (action.type) {
        case "FETCH_DATA_FACULTIES":
            return {...state, faculties: action.faculties};
        case "FETCH_DATA_SPECIALTIES":
            return {...state, specialties: action.specialties};
        case "FETCH_DATA_COURSES":
            return {...state, courses: action.courses};
        case "FETCH_DATA_TOPICS":
            return {...state, topics: action.topics}
        default:
            return state;
    }
}