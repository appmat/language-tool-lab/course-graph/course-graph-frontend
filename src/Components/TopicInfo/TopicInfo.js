import React from "react";
import {useState, useEffect} from "react";
import {Col, Container, Row} from "react-bootstrap";
import TopicHeader from "./TopicHeader";
import TopicList from "./TopicList";
import AddTopics from "./TopicInfoActions/AddTopics";
import defaultLink from "../Functions/Links";

async function getInfo(id){
    const response1 = await fetch(`${defaultLink}/api/subject/${id}`);
    const topicInfo = await response1.json();

    const response2 = await fetch(`${defaultLink}/api/subject/${id}/courses`);
    const coursesInfo = await response2.json();

    const response3 = await fetch(`${defaultLink}/api/subject/${id}/subjects/forward`);
    const topicsForward = await response3.json();

    const response4 = await fetch(`${defaultLink}/api/subject/${id}/subjects/backward`);
    const topicsBackward = await response4.json();

    return [topicInfo, coursesInfo, topicsForward, topicsBackward];
}

function TopicInfo(props){
    const [topic, setTopic] = useState([]);
    const [courses, setCourses] = useState([]);
    const [topicF, setTopicF] = useState([]);
    const [topicB, setTopicB] = useState([]);
    useEffect(()=>{
        if (topic.length === 0){
            getInfo(props.id).then((res)=>{
                setTopic(res[0]);
                setCourses(res[1]);
                setTopicF(res[2]);
                setTopicB(res[3]);
                document.title = "Тема: " + res[0].name;
            })
        }
    })

    return(
        <div className="specialty-page">
            <div className="courses-from-specialty-list-title">
                <Container style={{margin: "0", padding: "0", maxWidth: "87.75rem"}}>
                    <TopicHeader topic={topic}/>
                    <Row>
                        <div style={topicB.length === 0 ? {display: "none"} : {marginTop: "1rem"}}>
                            <h5>Темы, от которых зависит эта тема:</h5>
                            <TopicList topics = {topicB} />
                        </div>
                        <div style={{display: "flex", alignItems: "flex-end", marginBottom: "1rem"}}>
                            <h5>Темы, которые зависят от этой темы:</h5>
                            <AddTopics topic={topic} currentTopics={topicF} deprecatedTopics={topicB.concat({subId: topic.id, name: topic.name})}/>
                        </div>
                        <div style={topicF.length === 0 ? {display: "none"} : {}}>
                            <TopicList topics={topicF} />
                        </div>

                    </Row>
                    <Row style={{marginTop: "1rem"}}>
                        <Col>
                            <h5>Есть в курсах: {courses.length ? "" : "нет"}</h5>
                            <ul>
                                {courses.map((item,id)=>{
                                    return(
                                        <li key={id}> {item.name} </li>
                                    )
                                })}
                            </ul>
                        </Col>
                    </Row>
                </Container>

            </div>
        </div>
    )
}

export default TopicInfo;