import React, {useEffect, useState} from "react";
import {Button, Col, Container, Form, Modal, Row} from "react-bootstrap";
import {Typeahead} from "react-bootstrap-typeahead";
import { fetchTopics } from "../../Store/Actions";
import PutData from "../../Functions/PutData";
import {useDispatch, useSelector} from "react-redux";

function AddTopics(props){
    const [show, setShow] = useState(false);
    const [items, setItems]=useState([]);
    const allTopics = useSelector((state)=>state.topics);
    const dispatch = useDispatch();
    const [topic, setTopic] = useState([]);
    const [avTopics, setAvTopics] = useState([]);

    useEffect(() => {
        if (!allTopics.length){
            dispatch(fetchTopics());
        }
        getAvailableTopics();
    },[allTopics.length, items]);

    const handleClose = () => {
        setShow(false);
        setItems(props.currentTopics);
    }

    const handleShow = () => {
        setShow(true);
        setItems(props.currentTopics);
        getAvailableTopics();
    }

    // получение списка курсов, которых нет
    const getAvailableTopics = () => {
        let currentTopics=[];
        let availableTopics=[];
        let deprecatedTopics=[];
        items.forEach(i => {
            currentTopics.push(i.subId);
        })
        props.deprecatedTopics.forEach(item=>{
            deprecatedTopics.push(item.subId);
        });
        allTopics.forEach(i => {
            if (!currentTopics.includes(i.id) && !deprecatedTopics.includes(i.id))
                availableTopics.push({subId: i.id, name: i.name});
        });
        setAvTopics(availableTopics);
    }

    const deleteItem = (id) => {
        const newItems=items.filter(item => item.subId !== id);
        setItems(newItems);
    }

    const handlePut=()=>{
        let topicsToAdd=[];
        items.forEach(item=>{
            const data={
                subId: item.subId
            }
            topicsToAdd.push(data);
        })
        //alert(JSON.stringify(topic))
        PutData(topicsToAdd, `/api/subject/${props.topic.id}/subjects`).then(()=>{
        });
    }

    const handleSubmit = () => {
        if (topic.length>0){
            const data = {
                subId:topic[0].subId,
                name: topic[0].name,
                isNew: true
            };
            if (items.findIndex(item => item.subId===data.subId && item.name===data.name)===-1){
                setItems([data, ...items]);
            }
            setTopic([]);
        }
    };

    return(
        <div>
            <Button variant={"light"}
                    style={{margin: "0 .5rem 0 .5rem"}}
                    onClick={handleShow}>
                Изменить темы
            </Button>
            <Modal show={show}
                   onHide={handleClose}
                   backdrop="static"
                   keyboard={false}
                   style={{paddingTop: "2rem"}}
                   size="lg"
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "center", paddingTop: "2rem"}}>
                    <h4>Темы, которые зависят от: {props.topic.name}</h4>
                </Modal.Header>
                <Modal.Body>
                    <div style={{display: "flex", justifyContent: "center"}}>
                        <Form id="add-topic-to-topic" onSubmit={handlePut}>
                            <Form.Group>
                                <div style={{display: "flex"}}>
                                    <Typeahead
                                        id="add-course-to-specialty"
                                        labelKey="name"
                                        onChange={setTopic}
                                        options={avTopics} // allTopics
                                        placeholder="Тема"
                                        selected={topic}
                                        emptyLabel="Совпадений не найдено"
                                        style={{width: "30rem"}}
                                    />
                                    <Button variant="outline-dark" style={{marginLeft: "1rem"}}
                                            onClick={(e)=>{e.preventDefault(); handleSubmit()}}
                                    >
                                        Добавить
                                    </Button>
                                </div>
                            </Form.Group>
                        </Form>
                    </div>

                    <Container style={{marginTop: "2rem"}}>
                        <Row style={items.length===0 ? {display: "none"} :
                            {alignItems: "center", textAlign: "center", paddingBottom: "1rem"}}
                        >
                            <Col lg="2"/>
                            <Col lg="1"/>
                            <Col lg="6" style={{fontWeight: "bold"}}>Название</Col>
                        </Row>
                    </Container>

                    <Container style={{maxHeight: "calc(100vh - 450px)", overflowY: "auto", marginTop: ".25rem", paddingLeft: "0"}}>
                        {items.map((item,id)=>{
                            return(
                                <Row key={id} style={{alignItems: "center", paddingBottom: ".5rem", textAlign: "center", paddingTop: "5px"}}>
                                    <Col lg="2"/>
                                    <Col xs lg="1" style={{textAlign: "right"}}>
                                        {id+1}
                                    </Col>
                                    <Col lg="6" style={{textAlign: "left", minWidth: "13rem", maxWidth: "22rem"}}>
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            <div>{item.name}</div>
                                            <div style={item.isNew ?
                                                {width: "20px", height: "20px",borderRadius: "0.25rem", backgroundColor: "rgb(233,233,234)", marginLeft: "10px"}
                                                : {}}/>
                                        </div>
                                    </Col>
                                    <Col xs lg="1">
                                        <Button variant="light" onClick={()=>deleteItem(item.subId)}
                                                style={{backgroundColor: "transparent"}}>
                                            <i className="fas fa-trash"/>
                                        </Button>
                                    </Col>
                                </Row>
                            )
                        })}
                    </Container>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button variant="dark" type="submit" form="add-topic-to-topic" >Готово</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default AddTopics;
