import React, {useState} from "react";
import DeleteData from "../../Functions/DeleteData";
import {Button, Modal} from "react-bootstrap";
import {Link} from "react-router-dom";

function DeleteTopic(props){
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleSubmit = () => {
        DeleteData(`/api/subject/${props.topic.id}`).then(()=>{
        })
    };

    return(
        <>
            <Button variant="light" className="edit" onClick={handleShow}
                    style={{backgroundColor: "transparent", border: "none"}}>
                <i className="fas fa-trash"/>
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{paddingTop: "6rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Удалить тему</h4>
                </Modal.Header>
                <Modal.Body style={{paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <p style={{textAlign: "center"}}>{props.topic.name}</p>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Link to="/topics"  className="specialty-name">
                        <Button variant="dark" onClick={handleSubmit}>Удалить</Button>
                    </Link>
                </Modal.Footer>
            </Modal>
        </>
    )
}
export default DeleteTopic;