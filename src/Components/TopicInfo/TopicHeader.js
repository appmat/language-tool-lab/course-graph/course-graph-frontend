import React from "react";
import {Button, Col, Row} from "react-bootstrap";
import {Link} from "react-router-dom";
import DeleteTopic from "./TopicInfoActions/DeleteTopic";
import RenameTopic from "./TopicInfoActions/RenameTopic";

// + (topic.acronym.length ? (" (" + topic.acronym + ")") : '')

function TopicHeader(props){
    const topic = props.topic;
    return(
        <Row style={{alignItems: "center", marginBottom: "1rem"}}>
            <Col xs lg="1">
                <Link to="/topics">
                    <Button variant="outline-dark">
                        Назад
                    </Button>
                </Link>
            </Col>
            <Col style={{paddingRight: "0"}}>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <h4 style={{textAlign: "right", wordBreak: "break-word", margin: 0}}>
                        {topic.name}
                    </h4>
                    <RenameTopic topic={topic} />
                    <DeleteTopic topic={topic} />
                </div>
            </Col>
            <Col xs lg="1"/>
        </Row>
    )
}
export default TopicHeader;