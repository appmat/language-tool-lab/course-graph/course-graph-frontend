import React from "react";
import {Link} from "react-router-dom";

function TopicList(props){
    const topics = props.topics;
    return(
        <div className="courses-from-specialty-list">
            {topics.map((item,id)=>{
                return(
                    <Link key={id} to={`/topics/${item.subId}`} target="_blank" className="specialty-name">
                        <div className="course-card" style={{height: "4rem"}}>
                            <div className="topic-caption">
                                {item.name}
                            </div>
                        </div>
                    </Link>
                )
            })}
        </div>
    )
}

export default TopicList;