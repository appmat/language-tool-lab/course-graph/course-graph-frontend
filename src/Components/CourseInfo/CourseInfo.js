import React from "react";
import './CourseInfoList.css'
import {Container, Col, Row} from "react-bootstrap";
import CourseHeader from "./CourseHeader";
import AddTopicsToCourse from "./CourseInfoActions/AddTopicsToCourse";
import {Link} from "react-router-dom";
import defaultLink from "../Functions/Links";

async function getInfo(id){
    const response1 = await fetch(`${defaultLink}/api/course/${id}`);
    const courseInfo= await response1.json();

    const response2 = await fetch(`${defaultLink}/api/course/${id}/specialties`);
    const specialtiesInfo = await response2.json();

    const response3 = await fetch(`${defaultLink}/api/course/${id}/subjects`);
    const subjectsInfo = await response3.json();

    return [courseInfo, specialtiesInfo, subjectsInfo];
}

class CourseInfo extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            isLoaded: false,
            course: {},
            specialties: [],
            subjects: []
        }
    }

    handleLoaded=()=>{
        this.setState({isLoaded: true})
    }

    componentDidMount() {
        getInfo(this.props.id)
            .then((res)=>{
                this.setState({
                    course: res[0].course,
                    specialties: res[1],
                    subjects: res[2]
                }, this.handleLoaded)
            })
    }

    render(){
        const isLoaded = this.state.isLoaded;
        if (!isLoaded){
            return <p/>
        }
        else {
            const course = this.state.course;
            const specialties = this.state.specialties;
            const subjects = this.state.subjects;
            document.title = "Курс: " + course.name;
            return (
                <div className="specialty-page">
                    <div className="courses-from-specialty-list-title">
                        <Container style={{margin: "0", padding: "0", maxWidth: "87.75rem"}}>
                            <CourseHeader course={course}/>
                            <Row style={{marginTop: "1rem"}}>
                                <AddTopicsToCourse courseId={course.id} currentTopics={subjects}/>
                            </Row>
                            <Row style={{marginTop: "2rem"}}>
                                <div className="courses-from-specialty-list">
                                    {subjects.map((item,id)=>{
                                        return(
                                            <Link key={id} to={`/topics/${item.id}`} target="_blank" className="specialty-name">
                                                <div key={id} className="course-card" style={{height: "4rem"}} title={item.name}>
                                                    <div className="topic-caption">
                                                        {item.name}
                                                    </div>
                                                </div>
                                            </Link>
                                        )})
                                    }
                                </div>
                            </Row>
                            <Row style={{marginTop: "1rem"}}>
                                <Col>
                                    <h5>Есть в специальностях: {specialties.length ? "" : "нет"}</h5>
                                    <ul>
                                        {specialties.map((item,id)=>{
                                            return(
                                                <li key={id}> {item.name} </li>
                                            )
                                        })}
                                    </ul>
                                </Col>
                            </Row>
                        </Container>

                    </div>
                </div>
            )
        }
    }
}

export default CourseInfo;