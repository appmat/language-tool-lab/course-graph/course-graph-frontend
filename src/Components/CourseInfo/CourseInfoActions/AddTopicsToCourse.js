import React, {useState, useEffect} from "react";
import {Button, Form, Modal, Row, Col, Container} from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import { useSelector, useDispatch } from "react-redux";
import { fetchTopics } from "../../Store/Actions";
import PutData from "../../Functions/PutData";

import 'react-bootstrap-typeahead/css/Typeahead.css';

function AddTopicsToCourse(props){
    const courseId = props.courseId;

    // получение списка всех тем
    const allTopics = useSelector((state)=>state.topics);
    const dispatch = useDispatch();

    const [avTopics, setAvTopics] = useState([]);
    const [topic, setTopic] = useState([]);

    // выбранные темы
    const [items, setItems]=useState(props.currentTopics);

    // получение списка тем, которых нет
    const getAvailableTopics = () => {
        let currentTopics=[];
        let availableTopics=[];
        items.forEach(i => {
            currentTopics.push(i.id);
        })
        allTopics.forEach(i => {
            // если id просматриваемого элемента нет в текущих курсах, пушим в доступные
            if (!currentTopics.includes(i.id))
                availableTopics.push(i);
        })
        setAvTopics(availableTopics);
    }

    useEffect(() => {
        if (!allTopics.length){
            dispatch(fetchTopics());
        }
        getAvailableTopics();
    },[allTopics.length, items]);

    // добавление курсов в список
    const handleSubmit = () => {
        if (topic.length>0){
            // сохранение введенных данных
            const data = {
                id:topic[0].id,
                name: topic[0].name,
                isNew: true
            };
            if (items.findIndex(item => item.id===data.id && item.name===data.name)===-1){
                setItems([data, ...items]);
            }
            // очистка поля ввода
            setTopic([]);
        }
    };
    // удаление из списка
    const deleteItem = (id, term) => {
        const newItems=items.filter(item => item.id !==id || item.term!==term);
        setItems(newItems);
    }

    // открытие/закрытие модального окна
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
        setItems(props.currentTopics);
    }
    const handleShow = () => {
        setShow(true);
        getAvailableTopics();
    }

    const handlePut=()=>{
        let topics=[];
        items.forEach(item=>{
            const data={
                subjectId: item.id
            }
            topics.push(data);
        })
        PutData(topics, `/api/course/${courseId}/subjects`).then(()=>{
        });
    }

    return(
        <div style={{display: "flex", justifyContent: "center"}}>
            <Button variant={"light"}
                    style={{margin: "0 .5rem 0 .5rem"}}
                    onClick={handleShow}>
                Изменить темы
            </Button>
            <Modal show={show}
                   onHide={handleClose}
                   backdrop="static"
                   keyboard={false}
                   style={{paddingTop: "2rem"}}
                   size="lg"
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "center", paddingTop: "2rem"}}>
                    <h4>Темы курса</h4>
                </Modal.Header>
                <Modal.Body>
                    <div style={{display: "flex", justifyContent: "center"}}>
                        <Form id="add-course-to-specialty" onSubmit={handlePut}>
                            <Form.Group>
                                <div style={{display: "flex"}}>
                                    <Typeahead
                                        id="add-course-to-specialty"
                                        labelKey="name"
                                        onChange={setTopic}
                                        options={avTopics} // allTopics
                                        placeholder="Тема"
                                        selected={topic}
                                        emptyLabel="Совпадений не найдено"
                                        style={{width: "30rem"}}
                                    />
                                    <Button variant="outline-dark" style={{marginLeft: "1rem"}}
                                            onClick={(e)=>{e.preventDefault(); handleSubmit()}}
                                    >
                                        Добавить
                                    </Button>
                                </div>
                            </Form.Group>
                        </Form>
                    </div>

                    <Container style={{marginTop: "2rem"}}>
                        <Row style={items.length===0 ? {display: "none"} :
                            {alignItems: "center", textAlign: "center", paddingBottom: "1rem"}}
                        >
                            <Col lg="2"/>
                            <Col lg="1"/>
                            <Col lg="6" style={{fontWeight: "bold"}}>Название</Col>
                        </Row>
                    </Container>

                    <Container style={{maxHeight: "calc(100vh - 450px)", overflowY: "auto", marginTop: ".25rem", paddingLeft: "0"}}>
                        {items.map((item,id)=>{
                            return(
                                <Row key={id} style={{alignItems: "center", paddingBottom: ".5rem", textAlign: "center", paddingTop: "5px"}}>
                                    <Col lg="2"/>
                                    <Col xs lg="1" style={{textAlign: "right"}}>
                                        {id+1}
                                    </Col>
                                    <Col lg="6" style={{textAlign: "left", minWidth: "13rem", maxWidth: "22rem"}}>
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            <div>{item.name}</div>
                                            <div style={item.isNew ?
                                                {width: "20px", height: "20px",borderRadius: "0.25rem", backgroundColor: "rgb(233,233,234)", marginLeft: "10px"}
                                                : {}}/>
                                        </div>
                                    </Col>
                                    <Col xs lg="1">
                                        <Button variant="light" onClick={()=>deleteItem(item.id, item.term)}
                                                style={{backgroundColor: "transparent"}}>
                                            <i className="fas fa-trash"/>
                                        </Button>
                                    </Col>
                                </Row>
                            )
                        })}
                    </Container>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button variant="dark" type="submit" form="add-course-to-specialty" >Готово</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default AddTopicsToCourse;
