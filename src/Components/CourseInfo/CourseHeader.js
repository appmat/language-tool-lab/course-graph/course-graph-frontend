import React from "react";
import {Button, Col, Row} from "react-bootstrap";
import {Link} from "react-router-dom";
import RenameCourse from "./CourseInfoActions/RenameCourse";
import DeleteCourse from "./CourseInfoActions/DeleteCourse";

function CourseHeader(props){
    const course = props.course;
    return(
        <Row style={{alignItems: "center"}}>
            <Col xs lg="1">
                <Link to="/courses">
                    <Button variant="outline-dark">
                        Назад
                    </Button>
                </Link>
            </Col>
            <Col style={{paddingRight: "0"}}>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <h4 style={{textAlign: "right", wordBreak: "break-word"}}>
                        {course.name + (course.acronym.length ? (" (" + course.acronym + ")") : '')}
                    </h4>
                    <RenameCourse id={course.id} course={course}/>
                    <DeleteCourse id={course.id} name={course.name}/>
                </div>
            </Col>
            <Col xs lg="1"/>
        </Row>
    )
}
export default CourseHeader;