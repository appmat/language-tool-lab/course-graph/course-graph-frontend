import React from "react";
import './CoursesByLetterList.css';
import {Link} from "react-router-dom";

class CoursesByLetterList extends React.Component{
    render() {
        const letter=this.props.letter;
        const courses=this.props.courses;
        return(
            <div className="courses-by-letter-section">
                <h4 className="course-letter">{letter}</h4>
                <div className="courses-by-letter-list">
                    {courses.map( (item, i) => {
                        return(
                            <Link key={i} to={`/courses/${item.id}`} className="specialty-name">
                                <div title={item.name} className="course-card courses-by-letter-panel" key={i} style={{ width: '16.75rem', height: "5.4rem" }}>
                                    <div className="course-caption">
                                        {item.name}
                                    </div>
                                </div>
                            </Link>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default CoursesByLetterList;