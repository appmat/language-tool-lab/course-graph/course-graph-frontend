import React from "react";
import './AllCoursesList.css';
import CoursesByLetterList from "./CoursesByLetterList/CoursesByLetterList";

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {fetchCourses} from '../Store/Actions';

import SortByFirstLetter from "../Functions/SortByFirstLetter";

class AllCoursesList extends React.Component{
    componentDidMount() {
        this.props.fetchCourses();
    }

    componentWillUnmount() {
        this.props.fetchCourses();
    }

    render() {
        const courseList = SortByFirstLetter(this.props.courses);
        return(
            <div className="all-courses-page">
                <h2>База курсов</h2>
                {Object.entries(courseList)
                    .map(([key, value], i) => {
                        return(
                            <CoursesByLetterList key={i} letter={key} courses={value}/>
                        )
                    })}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        courses: state.courses
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        fetchCourses: bindActionCreators(fetchCourses, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllCoursesList);