import React from 'react';
import './SpecialtyList.css';
import {Link} from "react-router-dom";
import defaultLink from "../../Functions/Links";

async function getInfo(id){
    const response = await fetch(`${defaultLink}/api/faculty/${id}/specialties`);
    return await response.json();
}

class SpecialtyList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            specialties: []
        }
    }

    componentDidMount() {
        getInfo(this.props.id).then((res)=>{
            this.setState({specialties: res});
        })
    }

    render() {
        const spec = this.state.specialties;
        return(
            <div className="specialty-list">
                {spec.map((item)=>{
                    return(
                        <Link key={item.id} to={`/specialty/${item.id}`} className=" specialty-name">
                            <div title={item.name} style={{ width: '16.25rem', height: "6rem" }} className="course-card specialty-panel">
                                <div>
                                    {item.name}
                                </div>
                            </div>
                        </Link>
                    )
                })}
            </div>
        )
    }
}

export default SpecialtyList;

