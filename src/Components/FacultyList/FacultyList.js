import React from 'react';
import './FacultyList.css';
import { connect } from 'react-redux';
import { fetchFaculties } from '../Store/Actions';
import { bindActionCreators } from "redux";

import SpecialtyList from "./SpecialtyList/SpecialtyList";
import DeleteFaculty from "./FacultyActions/DeleteFaculty";
import RenameFaculty from "./FacultyActions/RenameFaculty";

class FacultyList extends React.Component{
    componentDidMount() {
        this.props.fetchFaculties();
    }

    render() {
        const fac = this.props.faculties;
        return(
            <div className="specialty-page">
                <h2>База факультетов и специальностей</h2>
                {fac.map((item, k)=>{
                    const id = item.id;
                    const name = item.name;
                    const acronym = item.acronym;
                    return(
                        <div key={k}>
                            <ul className="list-title-institute">
                                <li>
                                    <h4 className="title-institute"> {name + ( (acronym && acronym !== 'null') ? " (" + acronym + ")" : '')}</h4>
                                </li>
                                <li style={{paddingLeft: "1rem"}}>
                                    <RenameFaculty id={id} fac={item} />
                                    <DeleteFaculty id={id} fac={item}/>
                                </li>
                            </ul>
                            <SpecialtyList id={id}/>
                        </div>
                    )
                })}
                <p/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        faculties: state.faculties
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        fetchFaculties: bindActionCreators(fetchFaculties, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FacultyList)