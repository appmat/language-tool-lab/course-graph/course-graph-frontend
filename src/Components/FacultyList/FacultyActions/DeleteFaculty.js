import React, {useState} from "react";
import {Modal, Button} from "react-bootstrap";
import DeleteData from "../../Functions/DeleteData";

function DeleteFaculty(props){
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true);
    const handleSubmit = () => {
        DeleteData(`/api/faculty/${props.id}`).then(()=>{
            window.location.reload();
        })
        setShow(false);
    };

    return(
        <>
            <Button variant="light" className="edit" onClick={handleShow}
                    style={{/*display: "none",*/ backgroundColor: "transparent", border: "none"}}>
                <i className="fas fa-trash"/>
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{paddingTop: "6rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Удалить факультет</h4>
                </Modal.Header>
                <Modal.Body style={{paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <p style={{textAlign: "center"}}>{props.name}</p>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button variant="dark" type="submit" onClick={handleSubmit}>Удалить</Button>
                </Modal.Footer>

            </Modal>
        </>
    )
}

export default DeleteFaculty;