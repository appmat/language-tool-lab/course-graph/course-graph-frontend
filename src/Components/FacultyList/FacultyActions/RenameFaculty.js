import React, {useState} from "react";
import {Modal, Button, Form} from "react-bootstrap";
import PutData from "../../Functions/PutData";

function RenameFaculty(props){
    const [show, setShow] = useState(false);
    const [name, setName] = useState(props.fac.name);
    const [validated, setValidated] = useState(false);
    const [acronym, setAcronym] = useState(props.fac.acronym);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }else{
            const data={
                id: props.id,
                name: name,
                acronym: acronym
            };
            PutData(data, `/api/faculty/${data.id}`).then();
        }
        setValidated(true);
    };

    return(
        <>
            <Button title="Изменить название" variant="light" className="edit" onClick={handleShow} style={{backgroundColor: "transparent", border: "none"}}>
                <i className="fas fa-pen"/>
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                size="lg"
                style={{paddingTop: "6rem"}}
            >
                <Modal.Header style={{borderBottom: "none", justifyContent: "space-around", paddingTop: "2rem"}}>
                    <h4>Редактирование</h4>
                </Modal.Header>
                <Modal.Body style={{paddingLeft: "3rem", paddingRight: "3rem"}}>
                    <Form id="renameFaculty" noValidate validated={validated} onSubmit={handleSubmit}>
                        <Form.Group style={{paddingBottom: ".75rem"}}>
                            <Form.Control type="text" placeholder="Введите название" required value={name}
                                          onChange={(e) => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Сокращение" value={acronym}
                                          onChange={(e) => setAcronym(e.target.value)}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer style={{borderTop: "none", justifyContent: "center", paddingBottom: "2rem"}}>
                    <Button form="renameFaculty" variant="outline-dark" onClick={handleClose}> Отмена </Button>
                    <Button form="renameFaculty" variant="dark" type="submit">Подтвердить</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}
export default RenameFaculty