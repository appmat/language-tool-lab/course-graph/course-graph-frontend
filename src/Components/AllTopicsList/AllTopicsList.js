import React from "react";
//import './AllTopicsList.css';
import TopicsByLetterList from "./TopicsByLetterList";

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {fetchTopics} from '../Store/Actions';

import SortByFirstLetter from "../Functions/SortByFirstLetter";

class AllTopicsList extends React.Component{
    componentDidMount() {
        this.props.fetchTopics();
    }

    render() {
        const topicList = SortByFirstLetter(this.props.topics);
        return(
            <div className="all-courses-page">
                <h2>База тем</h2>
                {Object.entries(topicList)
                    .map(([key, value], i) => {
                        return(
                            <TopicsByLetterList key={i} letter={key} topics={value}/>
                        )
                    })}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        topics: state.topics
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        fetchTopics: bindActionCreators(fetchTopics, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllTopicsList);