import React from "react";
import {Link} from "react-router-dom";

class TopicsByLetterList extends React.Component{
    render() {
        const letter=this.props.letter;
        const topics=this.props.topics;

        return(
            <div className="courses-by-letter-section">
                <h4 className="course-letter">{letter}</h4>
                <div className="courses-by-letter-list">
                    {topics.map( (item, i) => {
                        return(
                            <Link key={i} to={`/topics/${item.id}`} className="specialty-name">
                                <div title={item.name} style={{ width: '16.25rem', height: "6rem" }} className="course-card courses-by-letter-panel">
                                    <div>
                                        {item.name}
                                    </div>
                                </div>
                            </Link>
                        )
                    })}
                </div>
            </div>
        )
    }

}

export default TopicsByLetterList;