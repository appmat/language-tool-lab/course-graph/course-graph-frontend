import React from "react";
import TopicInfo from "../Components/TopicInfo/TopicInfo";
import {useParams} from "react-router-dom";

function Topic(){
    let {id} = useParams();
    return(
        <div>
            <TopicInfo id={id}/>
        </div>
    )
}

export default Topic;