import React from "react";
import CoursesFromSpecialtyList from "../Components/CoursesFromSpecialtyList/CoursesFromSpecialtyList";
import {useParams} from "react-router-dom"

function CoursesFromSpecialty(){
    let {id} = useParams();
    return(
        <>
            <CoursesFromSpecialtyList id={id}/>
        </>
    )
}

export default CoursesFromSpecialty;