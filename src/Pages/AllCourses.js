import React from "react";
import AllCoursesList from "../Components/AllCoursesList/AllCoursesList";

function AllCourses(){
    document.title="База курсов";
    return(
        <>
            <AllCoursesList/>
        </>
    )
}

export default AllCourses;