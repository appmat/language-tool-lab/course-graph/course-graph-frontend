import React from "react";
import CourseInfo from '../Components/CourseInfo/CourseInfo'
import {useParams} from "react-router-dom"

function SubjectsFromCourse(){
    let {id} = useParams();
    return(
        <>
            <CourseInfo id={id}/>
        </>
    )
}

export default SubjectsFromCourse;