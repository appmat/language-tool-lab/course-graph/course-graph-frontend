import './App.css';
import React from "react";
import NavBar from "./Components/NavBar/NavBar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Faculties from "./Pages/Faculties";
import CoursesFromSpecialty from "./Pages/CoursesFromSpecialty";
import AllCourses from "./Pages/AllCourses";
import SubjectsFromCourse from "./Pages/SubjectsFromCourse";
import AllTopics from "./Pages/AllTopics";
import Topic from "./Pages/Topic";

function App() {
  return (
      <>
          <Router>
              <NavBar/>
              <Switch>
                  <Route exact path="/" component={Faculties}/>
                  <Route exact path={'/specialty/:id'} component={CoursesFromSpecialty}/>
                  <Route exact path="/courses" component={AllCourses}/>
                  <Route exact path={'/courses/:id'} component={SubjectsFromCourse}/>
                  <Route exact path={'/topics'} component={AllTopics}/>
                  <Route exact path={'/topics/:id'} component={Topic} />
              </Switch>
          </Router>
      </>
  );
}

export default App;
